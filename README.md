# AgVoy

The repository is a WIP.

It is licensed under the MIT license. 

AgVoy is a basic Symfony 4.3 app using webpack-encore for managing SASS and JS packages.
It is optimized for MySQL.

## Installation

First, clone the project onto your projects folder

```shell script
cd my-projects/
git clone https://gitlab.com/simtrami/agvoy.git
cd agvoy
```

For now, there are two ways to configure and launch the app:

- [The old way](#the-old-way)
- [With Docker](#with-docker)

### Requirements

It differs from the way you want to install the app.

### The old way

Requirements:

- PHP 7.2+ (installation heavily depends on your OS)
- [Composer](https://getcomposer.org/download/)
- MySQL v.5.7+ or MariaDB v.10+ (installation heavily depends on your OS)
- NPM v.6+ (installation heavily depends on your OS)

#### Installing the dependencies

The dependencies need to be installed.

```shell script
composer install
npm install
```

#### Compiling the assets

The assets (js and css) need to be compiled with Webpack.

For this you have the choice to run them minified for production purpose:

```shell script
npm run prod
```

Or if you're going to edit the files, start the watch script:

```shell script
npm run watch
```

#### Configuration

Copy .env.dist into a .env file and edit the configuration to match your database settings
(the database must have been created before).

Fill in the `APP_SECRET` entry with the return of the following command:
`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 40 | head -n 1`

Now you can load migrations into the database and import the fixtures.

```shell script
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```

#### Launching the app

Finally you can run the integrated web server

```shell script
php bin/console server:run
```

The site can be accessed by copying the returned link in your web browser.

### With Docker

You need to have Docker CE and Docker Compose installed.

Optionally you may want to have npm installed on your computer if you plan to
edit js or css files.

Requirements:

- [Docker CE](https://docs.docker.com/install/#supported-platforms)
- [Docker Compose](https://docs.docker.com/compose/)

#### Deploying the containers

Simply run `docker-compose up`.
It may take some time as it will instantiate every dependencies and run the Dockerfiles.

#### Launching the app

You know when the app is launched and ready to handle requests when both containers status are up and healthy.
You can check that with the following command (Ctrl+C to exit). 
```shell script
watch -n1 'docker container ls | grep agvoy'
``` 

Then the app is already running and accessible on [127.0.0.1:8000](http://127.0.0.1:8000).

#### Accessing the database

If you want to access your database from your host machine,
you can log into it with the following credentials:

- host: 127.0.0.1
- port: 33060
- user: docker
- password: secret
- database: docker
