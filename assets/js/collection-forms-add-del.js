const $ = require('jquery');

// Get the div that holds the collection
const $collectionHolder = $('div[data-prototype]');

function manageCollection(after = null, alterNewForm = null, afterAdd = null) {
    // setup an add button
    const $addButtonText = $collectionHolder.data('add-text');
    console.log($addButtonText);
    const $addButton = $('<button type="button" class="btn btn-outline-secondary btn-sm">' + $addButtonText + '</button>');
    const $newLinkElt = $('<div class="add"></div>').append($addButton);

    // add the add button anchor to the collected forms div
    $collectionHolder.append($newLinkElt);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    // add a delete button to all of the existing forms in collection (see last code block)
    // each form is contained as a first-level child of $collectionHolder in a fieldset tag
    $collectionHolder.children('fieldset').each(function () {
        addFormDelButton($(this), $collectionHolder);
    });

    $addButton.on('click', function (e) {
        // add a new form to the collection (see next code block)
        addTagForm($collectionHolder, $newLinkElt, alterNewForm, afterAdd);
    });

    if (after) after($collectionHolder);
}

function addTagForm($collectionHolder, $newLinkDiv, alterNewForm = null, afterAdd = null) {
    // Get the data-prototype explained earlier
    const prototype = $collectionHolder.data('prototype');

    // get the new number of elements in the collection
    const index = $collectionHolder.data('index');

    let newForm = prototype;
    // You need this only if you didn't set 'label' => false in your children field in the collected Type class
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    if (alterNewForm) newForm = alterNewForm(newForm);

    // Display the form in the page in a div, before the .add div
    let $newFormDiv = $('<div class="collected"></div>').append(newForm);
    $newLinkDiv.before($newFormDiv);

    if (afterAdd) afterAdd();

    // add a button to remove the added form if the user wants to cancel the upload (see next code block)
    addFormDelButton($newFormDiv, $collectionHolder);
}

function addFormDelButton($formDiv, $collectionHolder) {
    // setup a delete button
    const $delButtonText = $collectionHolder.data('del-text');
    const $removeButton = $('<div class="text-right"><button type="button" class="btn btn-outline-danger btn-sm mt-2">' + $delButtonText + '</button></div>');
    $formDiv.append($removeButton);

    $removeButton.on('click', function (e) {
        // remove the div, thus the form and if already persisted, the database stored element
        $formDiv.remove();
    });
}

export {manageCollection, $collectionHolder};
