// Library setting the selected file name in the input field
import bsCustomFileInput from "bs-custom-file-input";
import {manageCollection} from './collection-forms-add-del';

const $ = require('jquery');

$(document).ready(manageCollection(afterManage, alterNewForm, afterAdd));

function afterManage($collectionHolder) {
    $collectionHolder.find('.custom-file-label').each(function () {
        if (!$(this).text()) {
            let a = $(this).parent().parent().find('a');
            let filename = a.attr('href').split('/').pop();
            // Set the input text to the stored file name
            $(this).text(filename);
            // Set the img alt to the stored file name
            a.find('img').attr('alt', filename);
        }
    });

    // Instantiate bsCustomFileInput for the preexisting RoomPictureType forms
    bsCustomFileInput.init();
}

function alterNewForm(newForm) {
    // Sets the custom-file-input placeholder
    newForm = newForm.replace(/><\/label>/g, '>.png/.jpg/.jpeg</label>');

    return newForm;
}

function afterAdd() {
    // Re-instantiate bsCustomFileInput for the added RoomPictureType form
    bsCustomFileInput.init();
}
