FROM php:cli
RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils debconf git apt-transport-https \
    wget libzip-dev zlib1g-dev zip unzip
RUN docker-php-ext-install pdo_mysql zip
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer
RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
