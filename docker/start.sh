#!/bin/bash

# Move to the project directory (defined in docker-compose.yml at symfony.volumes)
# The container will stop if the directory doesn't exist
cd /var/www/symfony || exit

# Generates the app secret and the .env.local file
sed s/APP_SECRET=/APP_SECRET="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 40 | head -n 1)"/ .env > .env.local

composer install && npm install
# vendor and node_modules belong to more or less to root for an obscure reason
# This should be a temporary fix
# Also var/cache/dev belongs to root though it will not cause any problem as its rw at least.
chmod -R 757 node_modules
chmod -R 757 vendor
# Compile with Webpack once
npm run dev

# Waits for the database to be ready
./docker/wait-for-it.sh database:3306 2> /dev/null
start=$?
while [ $start != 0 ]
do
  printf "Not ready for database transactions yet...\n"
  ./docker/wait-for-it.sh database:3306 2> /dev/null
  # wait-for-it sleeps for 15s by default before failing
  start=$?
done
printf "Ready for database transactions!\n"

# Runs migrations, loads fixtures and creates the admin user
php bin/console doc:mig:mig --ansi -n
php bin/console doc:fix:load --ansi -n
php bin/console make:admin admin@agvoy.docker secret

# Watches with Webpack in the background
# Everything might not be watched before the server starts
# that's why I prefer to run dev once before
npm run watch &
# Starting the local web server
exec /tini -- php -S 0.0.0.0:8000 -t "/var/www/symfony/public"
