<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *      min = 2,
     *      max = 1000,
     *      minMessage = "It must be at least {{ limit }} characters long",
     *      maxMessage = "It cannot be longer than {{ limit }} characters"
     * )
     */
    private $content;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Range(
     *      min = 1,
     *      max = 5,
     *      minMessage = "Rating not in range",
     *      maxMessage = "Rating not in range"
     * )
     */
    private $rate;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $accepted = false;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $moderationRequested = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $room;

    /**
     * @var DateTime $writtenAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $writtenAt;

    /**
     * @var DateTime $moderatedAt
     *
     * @Gedmo\Timestampable(on="change", field={"content"})
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $moderatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function setRate(int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    public function getModerationRequested(): ?bool
    {
        return $this->moderationRequested;
    }

    public function setModerationRequested(bool $moderationRequested): self
    {
        $this->moderationRequested = $moderationRequested;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getWrittenAt(): ?DateTimeInterface
    {
        return $this->writtenAt;
    }

    public function getModeratedAt(): ?DateTimeInterface
    {
        return $this->moderatedAt;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }
}
