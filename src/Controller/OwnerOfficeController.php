<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\BookingType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/owner")
 * Class OwnerOfficeController
 * @package App\Controller
 */
class OwnerOfficeController extends AbstractController
{
    /**
     * @Route("/", name="oo_index")
     */
    public function index()
    {
        return $this->render('owner_office/index.html.twig');
    }

    /**
     * @Route("/bookings", name="oo_bookings")
     */
    public function bookings()
    {
        $rooms = $this->getUser()->getOwner()->getRooms();
        $bookings = array();

        foreach ($rooms as $room) {
            foreach ($room->getBookings() as $booking) {
                $bookings[] = $booking;
            }
        }

        return $this->render('owner_office/bookings.html.twig', [
            'bookings' => $bookings,
        ]);
    }

    /**
     * @Route("/bookings/{id}", name="oo_booking")
     * @param Booking $booking
     * @return Response
     */
    public function booking(Booking $booking)
    {
        if ($booking->getRoom()->getOwner()->getId() !== $this->getUser()->getOwner()->getId()) throw new AccessDeniedHttpException();

        return $this->render('owner_office/booking.html.twig', [
            'booking' => $booking,
        ]);
    }

    /**
     * @Route("/bookings/{id}/manage", name="oo_booking_manage")
     * @param Request $request
     * @param Booking $booking
     * @return Response
     */
    public function manageBooking(Request $request, Booking $booking)
    {
        if ($booking->getRoom()->getOwner()->getId() !== $this->getUser()->getOwner()->getId()) throw new AccessDeniedHttpException();

        $form = $this->createForm(BookingType::class, $booking, [
            'canSelectRoom' => false,
            'onlyConfirm' => true,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($booking);
            $em->flush();

            return $this->redirectToRoute('oo_booking', ['id' => $booking->getId()]);
        }

        return $this->render('owner_office/manage_booking.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
