<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Owner;
use App\Form\ClientType;
use App\Form\OwnerType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register()
    {
        return $this->render('security/register.html.twig');
    }

    /**
     * @Route("/register/client", name="app_register_client")
     * @param Request $request
     * @return Response
     */
    public function registerClient(Request $request)
    {
        $client = new Client();

        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $client->getUser();
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $user->getPassword()
            ));
            $user->setRoles(['ROLE_CLIENT']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($client);
            $entityManager->flush();

            $this->addFlash('info', "Client created successfully");
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/register_client.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/register/owner", name="app_register_owner")
     * @param Request $request
     * @return Response
     */
    public function registerOwner(Request $request)
    {
        $owner = new Owner();

        $form = $this->createForm(OwnerType::class, $owner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $owner->getUser();
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $user->getPassword()
            ));
            $user->setRoles(['ROLE_OWNER']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($owner);
            $entityManager->flush();

            $this->addFlash('info', "Owner created successfully");
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/register_owner.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // Prevent already logged users from login in again
        if ($this->getUser()) {
            if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
                return $this->redirectToRoute('back_office');
            } elseif (in_array('ROLE_OWNER', $this->getUser()->getRoles())) {
                return $this->redirectToRoute('home');
            } else {
                return $this->redirectToRoute('home');
            }
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        (!$error) ?: $this->addFlash('error', $error);
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     * @throws Exception
     */
    public function logout()
    {
        throw new Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/redirect-success", name="auth_success")
     * @return RedirectResponse
     */
    public function redirectAfterAuthSuccess()
    {
        $user = $this->getUser();

        if (!$user) throw new UsernameNotFoundException();

        if (in_array('ROLE_ADMIN', $user->getRoles())) {
            return $this->redirectToRoute('back_office');
        } elseif (in_array('ROLE_OWNER', $user->getRoles())) {
            return $this->redirectToRoute('home');
        } elseif (in_array('ROLE_CLIENT', $user->getRoles())) {
            return $this->redirectToRoute('co_index');
        }

        return $this->redirectToRoute('home');
    }
}
