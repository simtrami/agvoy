<?php

namespace App\Controller\BackOffice;

use App\Entity\Owner;
use App\Entity\Region;
use App\Entity\Room;
use App\Form\RoomType;
use App\Repository\RoomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/rooms")
 */
class RoomController extends AbstractController
{
    /**
     * @Route("/", name="room_index", methods={"GET"})
     * @param Request $request
     * @param RoomRepository $roomRepository
     * @return Response
     */
    public function index(Request $request, RoomRepository $roomRepository): Response
    {
        if ($request->query->has('region')) {
            $region = $this->getDoctrine()->getRepository(Region::class)->find($request->get('region'));
            if ($region) {
                $rooms = $region->getRooms();
            } else {
                throw new NotFoundHttpException('The requested region does not exist');
            }
        } elseif ($request->query->has('owner')) {
            $owner = $this->getDoctrine()->getRepository(Owner::class)->find($request->get('owner'));
            if ($owner) {
                $rooms = $owner->getRooms();
            } else {
                throw new NotFoundHttpException('The requested owner does not exist');
            }
        } else {
            $rooms = $roomRepository->findAll();
        }

        return $this->render('back_office/room/index.html.twig', [
            'rooms' => $rooms,
        ]);
    }

    /**
     * @Route("/new", name="room_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $room = new Room();
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();

            $this->addFlash('info', "Room created successfully");
            return $this->redirectToRoute('room_index');
        }

        return $this->render('back_office/room/new.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="room_show", methods={"GET"})
     * @param Room $room
     * @return Response
     */
    public function show(Room $room): Response
    {
        return $this->render('back_office/room/show.html.twig', [
            'room' => $room,
        ]);
    }

    /**
     * @param Request $request
     * @param Room $room
     * @return Response
     */
    public function edit(Request $request, Room $room): Response
    {
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('info', "Room updated successfully");
            return $this->redirectToRoute('room_index');
        }

        return $this->render('back_office/room/edit.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="room_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Room $room
     * @return Response
     */
    public function edit2(Request $request, Room $room): Response
    {
        // Create an ArrayCollection of the current RoomPicture objects in the database
        $originalPictures = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($room->getPictures() as $picture) {
            $originalPictures->add($picture);
        }

        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // remove the pictures if needed
            foreach ($originalPictures as $picture) {
                if (!$room->getPictures()->contains($picture)) {
                    $room->removePicture($picture);
                }
            }

            $em->persist($room);
            $em->flush();

            $this->addFlash('info', "Room updated successfully");
            return $this->redirectToRoute('room_index');
        }

        return $this->render('back_office/room/edit.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="room_delete", methods={"DELETE"})
     * @param Request $request
     * @param Room $room
     * @return Response
     */
    public function delete(Request $request, Room $room): Response
    {
        if ($this->isCsrfTokenValid('delete' . $room->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();
        }

        $this->addFlash('info', "Room removed successfully");
        return $this->redirectToRoute('room_index');
    }
}
