<?php

namespace App\Controller\BackOffice;

use App\Entity\Region;
use App\Entity\Room;
use App\Form\RegionType;
use App\Repository\RegionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/regions")
 */
class RegionController extends AbstractController
{
    /**
     * @Route("/", name="region_index", methods={"GET"})
     * @param Request $request
     * @param RegionRepository $regionRepository
     * @return Response
     */
    public function index(Request $request, RegionRepository $regionRepository): Response
    {
        if ($request->query->has('room')) {
            $room = $this->getDoctrine()->getRepository(Room::class)->find($request->get('room'));
            if ($room) {
                $regions = $room->getRegions();
            } else {
                throw new NotFoundHttpException('The requested room does not exist');
            }
        } else {
            $regions = $regionRepository->findAll();
        }

        return $this->render('back_office/region/index.html.twig', [
            'regions' => $regions,
        ]);
    }

    /**
     * @Route("/new", name="region_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $region = new Region();
        $form = $this->createForm(RegionType::class, $region);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($region);
            $entityManager->flush();

            $this->addFlash('info', "Region created successfully");
            return $this->redirectToRoute('region_index');
        }

        return $this->render('back_office/region/new.html.twig', [
            'region' => $region,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="region_show", methods={"GET"})
     * @param Region $region
     * @return Response
     */
    public function show(Region $region): Response
    {
        return $this->render('back_office/region/show.html.twig', [
            'region' => $region,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="region_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Region $region
     * @return Response
     */
    public function edit(Request $request, Region $region): Response
    {
        $form = $this->createForm(RegionType::class, $region);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('info', "Region updated successfully");
            return $this->redirectToRoute('region_index');
        }

        return $this->render('back_office/region/edit.html.twig', [
            'region' => $region,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="region_delete", methods={"DELETE"})
     * @param Request $request
     * @param Region $region
     * @return Response
     */
    public function delete(Request $request, Region $region): Response
    {
        if ($this->isCsrfTokenValid('delete' . $region->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($region);
            $entityManager->flush();
        }

        $this->addFlash('info', "Region removed successfully");
        return $this->redirectToRoute('region_index');
    }
}
