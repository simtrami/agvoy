<?php

namespace App\Controller\BackOffice;

use App\Entity\Booking;
use App\Entity\Client;
use App\Entity\Room;
use App\Form\BookingType;
use App\Repository\BookingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/bookings")
 */
class BookingController extends AbstractController
{
    /**
     * @Route("/", name="booking_index", methods={"GET"})
     * @param Request $request
     * @param BookingRepository $bookingRepository
     * @return Response
     */
    public function index(Request $request, BookingRepository $bookingRepository): Response
    {
        if ($request->query->has('room')) {
            $room = $this->getDoctrine()->getRepository(Room::class)->find($request->get('room'));
            if ($room) {
                $bookings = $room->getBookings();
            } else {
                throw new NotFoundHttpException('The requested room does not exist');
            }
        } elseif ($request->query->has('client')) {
            $client = $this->getDoctrine()->getRepository(Client::class)->find($request->get('client'));
            if ($client) {
                $bookings = $client->getBookings();
            } else {
                throw new NotFoundHttpException('The requested client does not exist');
            }
        } else {
            $bookings = $bookingRepository->findAll();
        }

        return $this->render('back_office/booking/index.html.twig', [
            'bookings' => $bookings,
        ]);
    }

    /**
     * @Route("/new", name="booking_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $booking = new Booking();
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($booking);
            $entityManager->flush();

            $this->addFlash('info', "Booking created successfully");
            return $this->redirectToRoute('booking_index');
        }

        return $this->render('back_office/booking/new.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="booking_show", methods={"GET"})
     * @param Booking $booking
     * @return Response
     */
    public function show(Booking $booking): Response
    {
        return $this->render('back_office/booking/show.html.twig', [
            'booking' => $booking,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="booking_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Booking $booking
     * @return Response
     */
    public function edit(Request $request, Booking $booking): Response
    {
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('info', "Booking edited successfully");
            return $this->redirectToRoute('booking_index');
        }

        return $this->render('back_office/booking/edit.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="booking_delete", methods={"DELETE"})
     * @param Request $request
     * @param Booking $booking
     * @return Response
     */
    public function delete(Request $request, Booking $booking): Response
    {
        if ($this->isCsrfTokenValid('delete' . $booking->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($booking);
            $entityManager->flush();
        }

        return $this->redirectToRoute('booking_index');
    }
}
