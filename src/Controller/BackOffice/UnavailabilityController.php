<?php

namespace App\Controller\BackOffice;

use App\Entity\Owner;
use App\Entity\Room;
use App\Entity\Unavailability;
use App\Form\UnavailabilityType;
use App\Repository\UnavailabilityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/unavailabilities")
 */
class UnavailabilityController extends AbstractController
{
    /**
     * @Route("/", name="unavailability_index", methods={"GET"})
     * @param Request $request
     * @param UnavailabilityRepository $unavailabilityRepository
     * @return Response
     */
    public function index(Request $request, UnavailabilityRepository $unavailabilityRepository): Response
    {
        if ($request->query->has('room')) {
            $room = $this->getDoctrine()->getRepository(Room::class)->find($request->get('room'));
            if ($room) {
                $unavailabilities = $room->getUnavailabilities();
            } else {
                throw new NotFoundHttpException('The requested room does not exist');
            }
        } elseif ($request->query->has('owner')) {
            $owner = $this->getDoctrine()->getRepository(Owner::class)->find($request->get('owner'));
            if ($owner) {
                $unavailabilities = $owner->getUnavailabilities();
            } else {
                throw new NotFoundHttpException('The requested owner does not exist');
            }
        } else {
            $unavailabilities = $unavailabilityRepository->findAll();
        }

        return $this->render('back_office/unavailability/index.html.twig', [
            'unavailabilities' => $unavailabilities,
        ]);
    }

    /**
     * @Route("/new", name="unavailability_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $unavailability = new Unavailability();
        $form = $this->createForm(UnavailabilityType::class, $unavailability);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($unavailability);
            $entityManager->flush();

            $this->addFlash('info', "Unavailability created successfully");
            return $this->redirectToRoute('unavailability_index');
        }

        return $this->render('back_office/unavailability/new.html.twig', [
            'unavailability' => $unavailability,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="unavailability_show", methods={"GET"})
     * @param Unavailability $unavailability
     * @return Response
     */
    public function show(Unavailability $unavailability): Response
    {
        return $this->render('back_office/unavailability/show.html.twig', [
            'unavailability' => $unavailability,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="unavailability_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Unavailability $unavailability
     * @return Response
     */
    public function edit(Request $request, Unavailability $unavailability): Response
    {
        $form = $this->createForm(UnavailabilityType::class, $unavailability);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('info', "Unavailability edited successfully");
            return $this->redirectToRoute('unavailability_index');
        }

        return $this->render('back_office/unavailability/edit.html.twig', [
            'unavailability' => $unavailability,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="unavailability_delete", methods={"DELETE"})
     * @param Request $request
     * @param Unavailability $unavailability
     * @return Response
     */
    public function delete(Request $request, Unavailability $unavailability): Response
    {
        if ($this->isCsrfTokenValid('delete' . $unavailability->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($unavailability);
            $entityManager->flush();
        }

        $this->addFlash('info', "Unavailability removed successfully");
        return $this->redirectToRoute('unavailability_index');
    }
}
