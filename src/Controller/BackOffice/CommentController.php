<?php

namespace App\Controller\BackOffice;

use App\Entity\Client;
use App\Entity\Comment;
use App\Entity\Room;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/comments")
 */
class CommentController extends AbstractController
{
    /**
     * @Route("/", name="comment_index", methods={"GET"})
     * @param Request $request
     * @param CommentRepository $commentRepository
     * @return Response
     */
    public function index(Request $request, CommentRepository $commentRepository): Response
    {
        if ($request->query->has('room')) {
            $room = $this->getDoctrine()->getRepository(Room::class)->find($request->get('room'));
            if ($room) {
                $comments = $room->getComments();
            } else {
                throw new NotFoundHttpException('The requested room does not exist');
            }
        } elseif ($request->query->has('client')) {
            $client = $this->getDoctrine()->getRepository(Client::class)->find($request->get('client'));
            if ($client) {
                $comments = $client->getComments();
            } else {
                throw new NotFoundHttpException('The requested client does not exist');
            }
        } else {
            $comments = $commentRepository->findAll();
        }

        return $this->render('back_office/comment/index.html.twig', [
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/new", name="comment_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            $this->addFlash('info', "Comment created successfully");
            return $this->redirectToRoute('comment_index');
        }

        return $this->render('back_office/comment/new.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comment_show", methods={"GET"})
     * @param Comment $comment
     * @return Response
     */
    public function show(Comment $comment): Response
    {
        return $this->render('back_office/comment/show.html.twig', [
            'comment' => $comment,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="comment_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Comment $comment
     * @return Response
     */
    public function edit(Request $request, Comment $comment): Response
    {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('info', "Comment updated successfully");
            return $this->redirectToRoute('comment_index');
        }

        return $this->render('back_office/comment/edit.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comment_delete", methods={"DELETE"})
     * @param Request $request
     * @param Comment $comment
     * @return Response
     */
    public function delete(Request $request, Comment $comment): Response
    {
        if ($this->isCsrfTokenValid('delete' . $comment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        $this->addFlash('info', "Comment removed successfully");
        return $this->redirectToRoute('comment_index');
    }
}
