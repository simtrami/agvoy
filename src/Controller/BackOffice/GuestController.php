<?php

namespace App\Controller\BackOffice;

use App\Entity\Booking;
use App\Entity\Guest;
use App\Form\GuestType;
use App\Repository\GuestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/guests")
 */
class GuestController extends AbstractController
{
    /**
     * @Route("/", name="guest_index", methods={"GET"})
     * @param Request $request
     * @param GuestRepository $guestRepository
     * @return Response
     */
    public function index(Request $request, GuestRepository $guestRepository): Response
    {
        if ($request->query->has('booking')) {
            $booking = $this->getDoctrine()->getRepository(Booking::class)->find($request->get('booking'));
            if ($booking) {
                $guests = $booking->getGuests();
            } else {
                throw new NotFoundHttpException('The requested booking does not exist');
            }
        } else {
            $guests = $guestRepository->findAll();
        }

        return $this->render('back_office/guest/index.html.twig', [
            'guests' => $guests,
        ]);
    }

    /**
     * @Route("/new", name="guest_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $guest = new Guest();
        $form = $this->createForm(GuestType::class, $guest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($guest);
            $entityManager->flush();

            return $this->redirectToRoute('guest_index');
        }

        return $this->render('back_office/guest/new.html.twig', [
            'guest' => $guest,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="guest_show", methods={"GET"})
     * @param Guest $guest
     * @return Response
     */
    public function show(Guest $guest): Response
    {
        return $this->render('back_office/guest/show.html.twig', [
            'guest' => $guest,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="guest_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Guest $guest
     * @return Response
     */
    public function edit(Request $request, Guest $guest): Response
    {
        $form = $this->createForm(GuestType::class, $guest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('guest_index');
        }

        return $this->render('back_office/guest/edit.html.twig', [
            'guest' => $guest,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="guest_delete", methods={"DELETE"})
     * @param Request $request
     * @param Guest $guest
     * @return Response
     */
    public function delete(Request $request, Guest $guest): Response
    {
        if ($this->isCsrfTokenValid('delete' . $guest->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($guest);
            $entityManager->flush();
        }

        return $this->redirectToRoute('guest_index');
    }
}
