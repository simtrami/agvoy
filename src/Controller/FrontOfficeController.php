<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Owner;
use App\Entity\Region;
use App\Entity\Room;
use App\Form\CommentRoomType;
use App\Repository\RegionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontOfficeController extends AbstractController
{
    /**
     * Get the sessions stored "likes" array
     * @return array
     */
    private function getLikes()
    {
        $likes = $this->get('session')->get('likes');
        if ($likes === null) {
            $likes = [];
        }
        return $likes;
    }

    /**
     * Overrides the AbstractController render function for always passing the "likes" array to the view template
     * @param string $view
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     */
    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        return parent::render($view, array_merge($parameters, ['likes' => $this->getLikes()]), $response);
    }

    /**
     * @Route("/", name="home", methods={"GET"})
     * @return Response
     */
    public function index()
    {
        return $this->render('front_office/index.html.twig');
    }

    /**
     * @Route("/about", name="about", methods={"GET"})
     * @return Response
     */
    public function about()
    {
        return $this->render('front_office/about.html.twig');
    }

    /**
     * Regions list
     * @Route("/explore", name="fo_list_regions", methods={"GET"})
     * @param RegionRepository $regionRepository
     * @return Response
     */
    public function regions(RegionRepository $regionRepository)
    {
        return $this->render('front_office/regions.html.twig', [
            'regions' => $regionRepository->findAll(),
        ]);
    }

    /**
     * Rooms of a region
     * @Route("/explore/r/{region}", name="fo_list_rooms_of_region", methods={"GET"})
     * @param Region $region
     * @return Response
     */
    public function regionRooms(Region $region): Response
    {
        return $this->render('front_office/region_rooms.html.twig', [
            'region' => $region,
        ]);
    }

    /**
     * Rooms of an owner
     * @Route("/o/{owner}", name="fo_list_rooms_of_owner", methods={"GET"})
     * @param Owner $owner
     * @return Response
     */
    public function ownerRooms(Owner $owner)
    {
        return $this->render('front_office/owner_rooms.html.twig', [
            'owner' => $owner,
        ]);
    }

    /**
     * Room page
     * @Route("/o/{owner}/{room}", name="fo_show_room", methods={"GET","POST"})
     * @param Request $request
     * @param Owner $owner
     * @param Room $room
     * @return Response
     */
    public function room(Request $request, Owner $owner, Room $room)
    {
        // Handle new Comment creation
        $comment = new Comment();
        $form = $this->createForm(CommentRoomType::class, $comment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $comment->setRoom($room);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $comment = new Comment();
            $form = $this->createForm(CommentRoomType::class, $comment);
            $this->addFlash('info', "Your comment is up for moderation, thank you for sharing your experience!");
        }

        return $this->render('front_office/room.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Add or remove a like
     * @Route("/o/{owner}/{room}/like", name="fo_update_room_like", methods={"GET"})
     * @param Owner $owner
     * @param Room $room
     * @return RedirectResponse
     */
    public function editLikes(Owner $owner, Room $room)
    {
        $likes = $this->getLikes();

        if (!in_array($room->getId(), $likes)) { // add like
            $likes[] = $room->getId();
            $this->addFlash('info', "You like this room.");
        } else { // remove like if already registered
            $likes = array_diff($likes, array($room->getId()));
            $this->addFlash('info', "You don't like this room anymore.");
        }
        $this->get('session')->set('likes', $likes);

        return $this->redirectToRoute('fo_show_room', [
            'owner' => $owner->getId(),
            'room' => $room->getId(),
        ]);
    }

    /**
     * Display liked rooms
     * @Route("/explore/likes", name="fo_liked_rooms", methods={"GET"})
     * @return Response
     */
    public function showLikes()
    {
        return $this->render('front_office/liked_rooms.html.twig', [
            'rooms' => $this->getDoctrine()->getRepository(Room::class)->findLiked($this->getLikes()),
        ]);
    }
}
