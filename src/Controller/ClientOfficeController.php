<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Room;
use App\Form\BookingType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/client")
 * Class ClientOfficeController
 * @package App\Controller
 */
class ClientOfficeController extends AbstractController
{
    /**
     * @Route("/", name="co_index")
     */
    public function index()
    {
        return $this->render('client_office/index.html.twig');
    }

    /**
     * @Route("/book", name="co_book", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function book(Request $request)
    {
        if (!$request->get('room')) throw new BadRequestHttpException();

        $room = $this->getDoctrine()->getRepository(Room::class)->find($request->get('room'));
        if (!$room) throw new NotFoundHttpException();

        $booking = new Booking();

        $form = $this->createForm(BookingType::class, $booking, [
            'canSelectRoom' => false,
            'canConfirm' => false,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO: Check if period is unavailable
            // TODO: Check if nb of guests <= room.capacity
            $booking->setRoom($room);
            $booking->setClient($this->getUser()->getClient());
            $em = $this->getDoctrine()->getManager();
            $em->persist($booking);
            $em->flush();

            $this->addFlash('success', 'You booked your room successfully, now you just have to wait for a response!');

            return $this->redirectToRoute('co_index');
        }

        return $this->render('client_office/book.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/bookings", name="co_bookings")
     */
    public function manageBookings()
    {
        $bookings = $this->getUser()->getClient()->getBookings();
        return $this->render('client_office/bookings.html.twig', [
            'bookings' => $bookings,
        ]);
    }

    /**
     * @Route("/bookings/{id}", name="co_booking")
     * @param Booking $booking
     * @return Response
     */
    public function showBooking(Booking $booking)
    {
        if ($booking->getClient()->getId() !== $this->getUser()->getClient()->getId()) throw new AccessDeniedHttpException();

        return $this->render('client_office/booking.html.twig', [
            'booking' => $booking,
        ]);
    }
}
