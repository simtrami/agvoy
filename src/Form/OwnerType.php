<?php

namespace App\Form;

use App\Entity\Owner;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OwnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $owner = $event->getData();
            $form = $event->getForm();

            // checks if the Owner object is "new"
            if (!$owner || null === $owner->getId()) {
                $form->add('user', AccountType::class, [
                    'label' => false,
                ]);
            } else {
                $form->add('user', EntityType::class, [
                    'required' => true,
                    'class' => User::class,
                    'choice_label' => 'email',
                ]);
            }
        });

        $builder
            ->add('name', TextType::class, [
                'required' => true,
            ])
            ->add('birthday', BirthdayType::class, [
                'required' => true,
            ])
            ->add('address', TextType::class, [
                'required' => true,
            ])
            ->add('country', TextType::class, [
                'required' => true,
                'attr' => [
                    'min' => 2,
                    'max' => 2,
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Owner::class,
        ]);
    }
}
