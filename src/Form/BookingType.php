<?php

namespace App\Form;

use App\Entity\Booking;
use App\Entity\Room;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $canSelectRoom = $options['canSelectRoom'];
        $canConfirm = $options['canConfirm'];
        $onlyConfirm = $options['onlyConfirm'];
        $builder
            ->add('startingOn', DateType::class, [
                'required' => true,
                'disabled' => $onlyConfirm,
            ])
            ->add('endingOn', DateType::class, [
                'required' => true,
                'disabled' => $onlyConfirm,
            ]);
        if ($canConfirm) {
            $builder
                ->add('confirmed', CheckboxType::class);
        }
        if ($canSelectRoom) {
            $builder
                ->add('room', EntityType::class, [
                    'required' => true,
                    'class' => Room::class,
                    'choice_label' => 'id',
                ]);
        }
        $builder
            ->add('guests', CollectionType::class, [
                'required' => true,
                'entry_type' => GuestType::class,
                'entry_options' => [
                    'label' => "Resident's information",
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'disabled' => $onlyConfirm,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'canSelectRoom' => true,
            'canConfirm' => true,
            'onlyConfirm' => false,
        ]);
    }
}
