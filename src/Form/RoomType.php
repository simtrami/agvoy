<?php

namespace App\Form;

use App\Entity\Owner;
use App\Entity\Region;
use App\Entity\Room;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('summary', TextareaType::class, [
                'required' => true,
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
            ])
            ->add('capacity', IntegerType::class, [
                'required' => true,
            ])
            ->add('superficy', NumberType::class, [
                'required' => true,
            ])
            ->add('price', MoneyType::class, [
                'required' => true,
            ])
            ->add('address', TextType::class, [
                'required' => true,
            ])
            ->add('animals', CheckboxType::class, [
                'required' => false,
            ])
            ->add('owner', EntityType::class, [
                'required' => true,
                'class' => Owner::class,
                'choice_label' => 'name',
            ])
            ->add('regions', EntityType::class, [
                'required' => true,
                'class' => Region::class,
                'choice_label' => 'name',
                'multiple' => true,
            ])
            ->add('pictures', CollectionType::class, [
                'required' => false,
                'entry_type' => RoomPictureType::class,
                'entry_options' => [
                    'label' => 'Choose a picture',
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
        ]);
    }
}
