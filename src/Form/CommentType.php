<?php

namespace App\Form;

use App\Entity\Comment;
use App\Entity\Room;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, [
                'required' => true,
            ])
            ->add('rate', RangeType::class, [
                'required' => true,
                'label' => 'Rate from 1 to 5',
                'attr' => [
                    'min' => 1,
                    'max' => 5,
                ]
            ])
            ->add('accepted', CheckboxType::class, [
                'required' => false,
            ])
            ->add('moderationRequested', CheckboxType::class, [
                'required' => false,
            ])
            ->add('room', EntityType::class, [
                'required' => true,
                'class' => Room::class,
                'choice_label' => 'id',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
