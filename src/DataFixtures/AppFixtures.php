<?php

namespace App\DataFixtures;

use App\Entity\Booking;
use App\Entity\Client;
use App\Entity\Comment;
use App\Entity\Guest;
use App\Entity\Owner;
use App\Entity\Region;
use App\Entity\Room;
use App\Entity\Unavailability;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;
    private $faker;

    private $regions = [];
    private $owners = [];
    private $clients = [];
    private $rooms = [];
    private $bookings = [];
    private $guests = [];
    private $comments = [];
    private $unavailabilities = [];

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        // On configure dans quelles langues nous voulons nos données
        $this->faker = Faker\Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager)
    {
        $this->createRegions(3, $manager);
        $this->createOwners(10, $manager);
        $this->createClients(10, $manager);
        $this->createRooms(10, $manager);
        $this->createComments(20, $manager);
        $this->createBookings(10, $manager);
        $this->createGuests(40, $manager);
        $this->createUnavailabilities(7, $manager);

        $manager->flush();
    }

    private function createRegions(int $nb, ObjectManager $manager)
    {
        for ($i = 0; $i < $nb; $i++) {
            $region = new Region();
            $region->setName($this->faker->region);
            $region->setPresentation($this->faker->sentence);
            $region->setCountry($this->faker->countryCode);
            $manager->persist($region);
            $this->regions[] = $region;
        }
    }

    private function createOwners(int $nb, ObjectManager $manager)
    {
        for ($i = 0; $i < $nb; $i++) {
            $owner = new Owner();
            $owner->setName($this->faker->name);
            $owner->setBirthday($this->faker->dateTime);
            $owner->setAddress($this->faker->address);
            $owner->setCountry($this->faker->countryCode);
            $user = $this->generateUser('secret', 'ROLE_OWNER');
            $owner->setUser($user);
            $manager->persist($owner);
            $this->owners[] = $owner;
        }
    }

    private function createClients(int $nb, ObjectManager $manager)
    {
        for ($i = 0; $i < $nb; $i++) {
            $client = new Client();
            $client->setName($this->faker->name);
            $client->setBirthday($this->faker->dateTime);
            $client->setCountry($this->faker->countryCode);
            $user = $this->generateUser('secret', 'ROLE_CLIENT');
            $client->setUser($user);
            $manager->persist($client);
            $this->clients[] = $client;
        }
    }

    private function createRooms(int $nb, ObjectManager $manager)
    {
        for ($i = 0; $i < $nb; $i++) {
            $room = new Room();
            $room->setSummary($this->faker->sentence);
            $room->setDescription($this->faker->sentence);
            $room->setCapacity($this->faker->numberBetween(1, 10));
            $room->setSuperficy($this->faker->randomFloat(2, 9, 250));
            $room->setPrice($this->faker->randomFloat(1, 1, 5000));
            $room->setAddress($this->faker->address);
            $room->setAnimals($this->faker->boolean);

            $room->setOwner($this->faker->randomElement($this->owners));
            $room->addRegion($this->faker->randomElement($this->regions));

            $manager->persist($room);
            $this->rooms[] = $room;
        }
    }

    private function createComments(int $nb, ObjectManager $manager)
    {
        for ($i = 0; $i < $nb; $i++) {
            $comment = new Comment();
            $comment->setContent($this->faker->sentence);
            $comment->setRate($this->faker->numberBetween(1, 5));
            $comment->setAccepted($this->faker->boolean);
            $comment->setModerationRequested($this->faker->boolean);

            $comment->setClient($this->faker->randomElement($this->clients));
            $comment->setRoom($this->faker->randomElement($this->rooms));

            $manager->persist($comment);
            $this->comments[] = $comment;
        }
    }

    private function createBookings(int $nb, ObjectManager $manager)
    {
        for ($i = 0; $i < $nb; $i++) {
            $booking = new Booking();
            $booking->setStartingOn($this->faker->dateTimeThisYear);
            $booking->setEndingOn($this->faker->dateTimeThisYear);
            $booking->setConfirmed($this->faker->boolean);

            $booking->setClient($this->faker->randomElement($this->clients));
            $booking->setRoom($this->faker->randomElement($this->rooms));

            $manager->persist($booking);
            $this->bookings[] = $booking;
        }
    }

    private function createGuests(int $nb, ObjectManager $manager)
    {
        for ($i = 0; $i < $nb; $i++) {
            $guest = new Guest();
            $guest->setName($this->faker->name);
            $guest->setBirthday($this->faker->dateTimeThisCentury);

            $guest->setBooking($this->faker->randomElement($this->bookings));

            $manager->persist($guest);
            $this->guests[] = $guest;
        }
    }

    private function createUnavailabilities(int $nb, ObjectManager $manager)
    {
        for ($i = 0; $i < $nb; $i++) {
            $unavailability = new Unavailability();
            $unavailability->setStartingOn($this->faker->dateTimeThisYear);
            $unavailability->setEndingOn($this->faker->dateTimeThisYear);

            $unavailability->setOwner($this->faker->randomElement($this->owners));
            $unavailability->setRoom($this->faker->randomElement($this->rooms));

            $manager->persist($unavailability);
            $this->unavailabilities[] = $unavailability;
        }
    }

    private function generateUser(String $plainPassword, ... $roles)
    {
        $user = new User();
        $user->setEmail($this->faker->unique()->email);
        $user->setRoles($roles);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $plainPassword));
        return $user;
    }
}
