<?php

namespace App\Repository;

use App\Entity\RoomPicture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RoomPicture|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoomPicture|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoomPicture[]    findAll()
 * @method RoomPicture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomPictureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoomPicture::class);
    }

    // /**
    //  * @return RoomPicture[] Returns an array of RoomPicture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoomPicture
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
