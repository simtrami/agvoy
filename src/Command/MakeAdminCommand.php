<?php

namespace App\Command;

use App\Entity\User;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MakeAdminCommand extends Command
{
    protected static $defaultName = 'make:admin';
    private $passwordEncoder;
    private $container;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, ContainerInterface $container, string $name = null)
    {
        parent::__construct($name);
        $this->passwordEncoder = $passwordEncoder;
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a new user with ROLE_ADMIN')
            ->addArgument('email', InputArgument::OPTIONAL, 'Email Address')
            ->addArgument('password', InputArgument::OPTIONAL, 'Password');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');

        if (!$email) {
            $io->error('You need to pass an email argument');
        }
        if (!$password) {
            $io->error('You need to pass a password argument');
        }

        $existingUser = $this->container->get('doctrine')
            ->getRepository(User::class)->findOneBy(['email' => $email]);

        if (!$existingUser) {
            $user = new User();
            $user->setEmail($email);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setRoles(['ROLE_ADMIN']);
            $msg = "The user {$user->getEmail()} was successfully created.";
        } elseif (in_array('ROLE_ADMIN', (array)$existingUser->getRoles())) {
            $user = $existingUser;
            $user->setRoles(
                array_merge(
                    ['ROLE_ADMIN'],
                    array_diff(['ROLE_USER'], (array)$existingUser->getRoles()
                    )
                )
            );
            $msg = "The user {$user->getEmail()} existed. It is now granted admin access.";
        } else {
            $io->error("The user {$existingUser->getEmail()} already exists and is admin.");
            return;
        }
        $manager = $this->container->get('doctrine')->getManager();
        $manager->persist($user);
        $manager->flush();
        $io->success($msg);
    }
}
